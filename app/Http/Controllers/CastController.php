<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;

class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama_artis' => 'required',
            'umur_artis' => 'required',
            'bio_artis' => 'required',
        ]);

        $cast = new Cast;
 
        $cast->nama = $request->nama_artis;
        $cast->umur = $request->umur_artis;
        $cast->bio = $request->bio_artis;

        $cast->save();
        return redirect('/cast');
    }

    public function index(){
        $cast = Cast::all(); 
        return view('cast.index', compact('cast'));
    }

    public function show($cast_id){
        $cast = cast::where('id', $cast_id)->first();
        return view('cast.show', compact('cast'));
    }

    public function edit($cast_id){
        $cast = cast::where('id', $cast_id)->first();
        return view('cast.edit', compact('cast'));
    }

    public function update(Request $request, $cast_id){
        $request->validate([
            'nama_artis' => 'required',
            'umur_artis' => 'required',
            'bio_artis' => 'required',
        ]);

        $cast = cast::find($cast_id);
 
        $cast->nama = $request->nama_artis;
        $cast->umur = $request->umur_artis;
        $cast->bio = $request->bio_artis;
 
        $cast->save();

        return redirect('/cast');
    }

    public function destroy($cast_id){
        $cast = Cast::find($cast_id);
 
        $cast->delete();

        return redirect('/cast');
    }
}
