<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@form');

Route::post('/welcome', 'AuthController@hasil');

Route::get('/data-tables', function(){
    return view('table.data-tables');
});

//CRUD Table Cast
//Create Data
Route::get('/cast/create', 'CastController@create'); //Route menampilkan form untuk membuat data pemain film baru
Route::post('/cast', 'CastController@store'); //Route menyimpan data baru ke tabel Cast

//Read Data
Route::get('/cast', 'CastController@index'); //Route menampilkan list data para pemain film 
Route::get('/cast/{cast_id}', 'CastController@show'); //Route menampilkan detail data pemain film dengan id tertentu

//update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route menampilkan form untuk edit pemain film dengan id tertentu
Route::put('/cast/{cast_id}', 'CastController@update'); //Route menyimpan perubahan data pemain film (update) untuk id tertentu

//delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //Route menghapus data pemain film dengan id tertentu