@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
    <h2>Buat Account Baru</h2>
    <h4>Sign Up Form</h4><br>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name :</label><br>
        <input type="text" name="fname"><br><br>
        <label>Last Name :</label><br>
        <input type="text" name="lname"><br><br>
        <label>Gender :</label><br>
        <input type="radio" name="gr" value="lk">Male<br>
        <input type="radio" name="gr" value="pr">Female<br>
        <input type="radio" name="gr" value="pr">other<br><br>
        <label>Nationality :</label><br>
        <Select name="nationality">
            <option value="none">-- select one --</option>
            <option value="afghan">Afghan</option>
            <option value="albanian">Albanian</option>
            <option value="algerian">Algerian</option>
            <option value="american">American</option>
            <option value="andorran">Andorran</option>
            <option value="antiguans">Antiguans</option>
            <option value="angolan">Angolan</option>
            <option value="armenian">Armenian</option>
            <option value="argentinean">Argentinean</option>
            <option value="austrian">Austrian</option>
            <option value="australian">Australian</option>
            <option value="bahamian">Bahamian</option>
            <option value="azerbaijani">Azerbaijani</option>
            <option value="bahraini">Bahraini</option>
            <option value="barbadian">Barbadian</option>
            <option value="bangladeshi">Bangladeshi</option>
            <option value="batswana">Batswana</option>
            <option value="barbudans">Barbudans</option>
            <option value="belgian">Belgian</option>
            <option value="belarusian">Belarusian</option>
            <option value="beninese">Beninese</option>
            <option value="belizean">Belizean</option>
            <option value="bolivian">Bolivian</option>
            <option value="bhutanese">Bhutanese</option>
            <option value="brazilian">Brazilian</option>
            <option value="bosnian">Bosnian</option>
            <option value="bruneian">Bruneian</option>
            <option value="british">British</option>
            <option value="burkinabe">Burkinabe</option>
            <option value="bulgarian">Bulgarian</option>
            <option value="burundian">Burundian</option>
            <option value="burmese">Burmese</option>
            <option value="cameroonian">Cameroonian</option>
            <option value="cambodian">Cambodian</option>
            <option value="cape verdean">Cape Verdean</option>
            <option value="canadian">Canadian</option>
            <option value="chadian">Chadian</option>
            <option value="central african">Central African</option>
            <option value="chinese">Chinese</option>
            <option value="colombian">Colombian</option>
            <option value="comoran">Comoran</option>
            <option value="congolese">Congolese</option>
            <option value="costa rican">Costa Rican</option>
            <option value="croatian">Croatian</option>
            <option value="cuban">Cuban</option>
            <option value="chilean">Chilean</option>
            <option value="czech">Czech</option>
            <option value="cypriot">Cypriot</option>
            <option value="djibouti">Djibouti</option>
            <option value="danish">Danish</option>
            <option value="dutch">Dutch</option>
            <option value="dominican">Dominican</option>
            <option value="ecuadorean">Ecuadorean</option>
            <option value="east timorese">East Timorese</option>
            <option value="emirian">Emirian</option>
            <option value="egyptian">Egyptian</option>
            <option value="eritrean">Eritrean</option>
            <option value="equatorial guinean">Equatorial Guinean</option>
            <option value="ethiopian">Ethiopian</option>
            <option value="estonian">Estonian</option>
            <option value="filipino">Filipino</option>
            <option value="fijian">Fijian</option>
            <option value="french">French</option>
            <option value="finnish">Finnish</option>
            <option value="gambian">Gambian</option>
            <option value="gabonese">Gabonese</option>
            <option value="georgian">Georgian</option>
            <option value="ghanaian">Ghanaian</option>
            <option value="german">German</option>
            <option value="grenadian">Grenadian</option>
            <option value="greek">Greek</option>
            <option value="guinea-bissauan">Guinea-Bissauan</option>
            <option value="guatemalan">Guatemalan</option>
            <option value="guyanese">Guyanese</option>
            <option value="guinean">Guinean</option>
            <option value="herzegovinian">Herzegovinian</option>
            <option value="haitian">Haitian</option>
            <option value="hungarian">Hungarian</option>
            <option value="honduran">Honduran</option>
            <option value="indian">Indian</option>
            <option value="icelander">Icelander</option>
            <option value="iranian">Iranian</option>
            <option value="indonesian">Indonesian</option>
            <option value="irish">Irish</option>
            <option value="iraqi">Iraqi</option>
            <option value="italian">Italian</option>
            <option value="israeli">Israeli</option>
            <option value="jamaican">Jamaican</option>
            <option value="ivorian">Ivorian</option>
            <option value="jordanian">Jordanian</option>
            <option value="kazakhstani">Kazakhstani</option>
            <option value="japanese">Japanese</option>
            <option value="kittian and nevisian">Kittian and Nevisian</option>
            <option value="kenyan">Kenyan</option>
            <option value="kyrgyz">Kyrgyz</option>
            <option value="kuwaiti">Kuwaiti</option>
            <option value="latvian">Latvian</option>
            <option value="laotian">Laotian</option>
            <option value="liberian">Liberian</option>
            <option value="lebanese">Lebanese</option>
            <option value="liechtensteiner">Liechtensteiner</option>
            <option value="libyan">Libyan</option>
            <option value="luxembourger">Luxembourger</option>
            <option value="lithuanian">Lithuanian</option>
            <option value="malagasy">Malagasy</option>
            <option value="macedonian">Macedonian</option>
            <option value="malaysian">Malaysian</option>
            <option value="malawian">Malawian</option>
            <option value="malian">Malian</option>
            <option value="maldivan">Maldivan</option>
            <option value="marshallese">Marshallese</option>
            <option value="maltese">Maltese</option>
            <option value="mauritian">Mauritian</option>
            <option value="mauritanian">Mauritanian</option>
            <option value="micronesian">Micronesian</option>
            <option value="mexican">Mexican</option>
            <option value="monacan">Monacan</option>
            <option value="moldovan">Moldovan</option>
            <option value="moroccan">Moroccan</option>
            <option value="mongolian">Mongolian</option>
            <option value="motswana">Motswana</option>
            <option value="mosotho">Mosotho</option>
            <option value="namibian">Namibian</option>
            <option value="mozambican">Mozambican</option>
            <option value="nepalese">Nepalese</option>
            <option value="nauruan">Nauruan</option>
            <option value="ni-vanuatu">Ni-Vanuatu</option>
            <option value="new zealander">New Zealander</option>
            <option value="nigerien">Nigerien</option>
            <option value="nicaraguan">Nicaraguan</option>
            <option value="northern irish">Northern Irish</option>
            <option value="north korean">North Korean</option>
            <option value="omani">Omani</option>
            <option value="norwegian">Norwegian</option>
            <option value="palauan">Palauan</option>
            <option value="pakistani">Pakistani</option>
            <option value="papua new guinean">Papua New Guinean</option>
            <option value="panamanian">Panamanian</option>
            <option value="peruvian">Peruvian</option>
            <option value="paraguayan">Paraguayan</option>
            <option value="portuguese">Portuguese</option>
            <option value="polish">Polish</option>
            <option value="romanian">Romanian</option>
            <option value="qatari">Qatari</option>
            <option value="rwandan">Rwandan</option>
            <option value="russian">Russian</option>
            <option value="salvadoran">Salvadoran</option>
            <option value="saint lucian">Saint Lucian</option>
            <option value="san marinese">San Marinese</option>
            <option value="samoan">Samoan</option>
            <option value="saudi">Saudi</option>
            <option value="sao tomean">Sao Tomean</option>
            <option value="senegalese">Senegalese</option>
            <option value="scottish">Scottish</option>
            <option value="seychellois">Seychellois</option>
            <option value="serbian">Serbian</option>
            <option value="singaporean">Singaporean</option>
            <option value="sierra leonean">Sierra Leonean</option>
            <option value="slovenian">Slovenian</option>
            <option value="slovakian">Slovakian</option>
            <option value="somali">Somali</option>
            <option value="solomon islander">Solomon Islander</option>
            <option value="south korean">South Korean</option>
            <option value="south african">South African</option>
            <option value="sri lankan">Sri Lankan</option>
            <option value="spanish">Spanish</option>
            <option value="surinamer">Surinamer</option>
            <option value="sudanese">Sudanese</option>
            <option value="swedish">Swedish</option>
            <option value="swazi">Swazi</option>
            <option value="syrian">Syrian</option>
            <option value="swiss">Swiss</option>
            <option value="tajik">Tajik</option>
            <option value="taiwanese">Taiwanese</option>
            <option value="thai">Thai</option>
            <option value="tanzanian">Tanzanian</option>
            <option value="tongan">Tongan</option>
            <option value="togolese">Togolese</option>
            <option value="tunisian">Tunisian</option>
            <option value="trinidadian or tobagonian">Trinidadian or Tobagonian</option>
            <option value="tuvaluan">Tuvaluan</option>
            <option value="turkish">Turkish</option>
            <option value="ukrainian">Ukrainian</option>
            <option value="ugandan">Ugandan</option>
            <option value="uzbekistani">Uzbekistani</option>
            <option value="uruguayan">Uruguayan</option>
            <option value="vietnamese">Vietnamese</option>
            <option value="venezuelan">Venezuelan</option>
            <option value="yemenite">Yemenite</option>
            <option value="welsh">Welsh</option>
            <option value="zimbabwean">Zimbabwean</option>
            <option value="zambian">Zambian</option>
        </Select><br><br>
        <label>Lenguage Spoken :</label><br>
        <input type="checkbox" name="lenguage" value="id">Bahasa Indonesia<br>
        <input type="checkbox" name="lenguage" value="eng">English<br>
        <input type="checkbox" name="lenguage" value="other">Other<br><br>
        <label>Bio :</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
@endsection