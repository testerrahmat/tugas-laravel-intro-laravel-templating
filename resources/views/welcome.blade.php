@extends('layout.master')

@section('judul')
Halaman Index
@endsection

@section('content')
    <h1>SELAMAT DATANG! {{$fname}} {{$lname}}</h1>
    <h5>Terima kasih telah bergabung di Website Kami. Media Belajar kita bersama!</h5>
@endsection