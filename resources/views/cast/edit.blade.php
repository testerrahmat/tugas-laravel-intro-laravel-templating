@extends('layout.master')

@section('judul')
Edit Pemain Film
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method("PUT")
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama_artis" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama_artis')
    <div class="alert alert-danger">Nama Pemain Film is Required</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur_artis" value="{{$cast->umur}}" class="form-control" min="1">
    </div>
    @error('umur_artis')
    <div class="alert alert-danger">Umur Pemain Film is Required</div>
    @enderror
    <div class="form-group">
      <label >Biodata</label>
      <textarea name="bio_artis" class="form-control">{{$cast->bio}}</textarea>
    </div>
    @error('bio_artis')
    <div class="alert alert-danger">Biodata Pemain Film is Required</div>
    @enderror
    <button type="submit" class="btn btn-primary">Save</button>
  </form>
@endsection