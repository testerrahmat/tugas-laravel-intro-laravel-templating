@extends('layout.master')

@section('judul')
List Pemain Film
@endsection

@section('content')

<a href="/cast/create" class="btn btn-secondary mb-3">Tambah Pemain Film Baru</a>
<table class="table" style="text-align: center">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col" class="col-md-5">Biodata</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
          <tr>
              <td>{{$key + 1}}</td>
              <td>{{$item->nama}}</td>
              <td>{{$item->umur}}</td>
              <td style="text-align:left">{{$item->bio}}</td>
              <td>
                <form action="cast/{{$item->id}}" method="POST">
                   <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a> 
                   <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                   @csrf
                   @method('delete')
                   <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
              </td>
          </tr>
      @empty
          <h1>Data tidak ditemukan</h1>
      @endforelse
    </tbody>
  </table>
  @endsection