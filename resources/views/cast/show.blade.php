@extends('layout.master')

@section('judul')
Detail Pemain Film
@endsection

@section('content')

<h5>Nama : </h5>
<p>{{$cast->nama}}</p>
<h5>Umur : </h5>
<p>{{$cast->umur}}</p>
<h5>Biodata :</h5>
<p>{{$cast->bio}}</p>

@endsection