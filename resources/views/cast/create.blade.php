@extends('layout.master')

@section('judul')
Tambah Pemain Film
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama_artis" class="form-control">
    </div>
    @error('nama_artis') 
    <div class="alert alert-danger">Nama Pemain Film is Required</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur_artis" class="form-control" min="1">
    </div>
    @error('umur_artis')
    <div class="alert alert-danger">Umur Pemain Film is Required</div>
    @enderror
    <div class="form-group">
      <label >Biodata</label>
      <textarea name="bio_artis" class="form-control"></textarea>
    </div>
    @error('bio_artis')
    <div class="alert alert-danger">Biodata Pemain Film is Required</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection